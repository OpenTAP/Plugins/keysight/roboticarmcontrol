﻿using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace OpenTap.Plugins.RoboticArmControl
{
    public class ModbusSocketServer
    {
        private const int SCRIPT_PORT = 30002;
        private Socket socket;
        private string remote_host; 

        public ModbusSocketServer(string host)
        {
            remote_host = host; 
            ConnectSocket();
        }

        public void ConnectSocket()
        {
            IPHostEntry hostEntry = null;

            // Get host related information.
            hostEntry = Dns.GetHostEntry(remote_host);

            // Loop through the AddressList to obtain the supported AddressFamily. This is to avoid
            // an exception that occurs when the host IP Address is not compatible with the address family
            // (typical in the IPv6 case).
            foreach (IPAddress address in hostEntry.AddressList)
            {
                IPEndPoint ipe = new IPEndPoint(address, SCRIPT_PORT);
                Socket tempSocket =
                    new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                tempSocket.Connect(ipe);

                if (tempSocket.Connected)
                {
                    socket = tempSocket;
                    break;
                }
                else
                {
                    continue;
                }
            }
        }

        public void DisconnectSocket()
        {
            socket.Close();
            socket.Dispose();
        }

        public void SendCommand(string cmd)
        {
            if (!socket.Connected)
            {
                ConnectSocket();
            }

            byte[] bytesSent = Encoding.ASCII.GetBytes(cmd);
            socket.Send(bytesSent, bytesSent.Length, 0);
            Thread.Sleep(2000);

            // after sending all the commands, read in the response data 
            byte[] bytesReceived = new byte[256];

            int bytes = 0;
            int totalBytes = 0;
            string response = "";

            do
            {
                bytes = socket.Receive(bytesReceived, 256, 0);
                response = response + Encoding.ASCII.GetString(bytesReceived, 0, bytes);
                totalBytes += bytes;
            }
            while (totalBytes < 1024);
        }

        public void SendCommands(string[] cmdArray)
        {
            for (int i = 0; i < cmdArray.Length; i++)
            {
                SendCommand(cmdArray[i]);
                Thread.Sleep(2000);
            }
        }
    }
}
