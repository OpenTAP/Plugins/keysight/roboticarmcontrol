﻿namespace OpenTap.Plugins.RoboticArmControl
{
    [Display("Reset", Group: "Robotic Arm Control", Description: "Insert a description here")]
    public class Reset : TestStep
    {

        [Display("Arm")]
        public IRoboticArm Arm { get; set; }

        public Reset()
        {
            // ToDo: Set default values for properties / settings.
        }

        public override void Run()
        {
            Arm.Reset();
            UpgradeVerdict(Verdict.Pass);
        }
    }
}
