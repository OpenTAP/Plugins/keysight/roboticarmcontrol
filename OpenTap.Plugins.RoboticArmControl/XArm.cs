﻿using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.IO;

namespace OpenTap.Plugins.RoboticArmControl 
{
    [Display("XArm", Group: "Robotic Arm Control", Description: "Example of controling a HID based Robotic Arm" )]
    public class XArm : Instrument, IRoboticArm
    {
        private XArmHid xArm;
        private class Command
        {
            public int Servo1;
            public int Servo2;
            public int Servo3;
            public int Servo4;
            public int Servo5;
            public int Servo6;
            public int Time;
            public int Index;
        };


        private string controlFile;

        [Display("Control File Location")]
        [FilePath]
        public string ControlFileLocation
        {
            get
            {
                return controlFile;
            }
            set
            {
                controlFile = value;
                ParseControlFile();
            }
        }

        [AvailableValues("AvailableXArmAddresses")]
        public string Address { get; set; }

        [Browsable(false)]
        public List<string> AvailableXArmAddresses => XArmHid.Discover();

        private Dictionary<string, Dictionary<int, List<Command>>> Sequences;

        public XArm()
        {
            Name = "xArm";
            Address = AvailableXArmAddresses.FirstOrDefault();
            Sequences = new Dictionary<string, Dictionary<int, List<Command>>>();
        }

        public override void Open()
        {
            xArm = new XArmHid();
            xArm.Connect(Address);
            Reset();
            DownloadGroups();
            base.Open();
        }

        public override void Close()
        {
            xArm.Disconnect();
            base.Close();
        }

        public void PlayAction(string dutType, string dutLocation)
        {
            PlayActionGroup(Sequences[$"{dutType}:{dutLocation}"]);
        }

        public void Move(List<double> position)
        {
            xArm.ServoMoveAll(1000, (int)position[0], (int)position[1], (int)position[2], (int)position[3], (int)position[4], (int)position[5]);
        }

        public void MoveJoint(int jointId, double position)
        {
            xArm.ServoMove(jointId, (int)position);
        }

        public void Reset()
        {
            xArm.Reset();
        }

        public List<string> GetLocations()
        {
            ParseControlFile();
            return Sequences.Keys.Distinct().Select(x => x.Split(':')[1]).ToList();
        }

        public List<string> GetDutTypes()
        {
            ParseControlFile();
            return Sequences.Keys.Distinct().Select(x => x.Split(':')[0]).ToList();
        }

        private void PlayActionGroup(Dictionary<int, List<Command>> sequence)
        {
            xArm.PlayGroup(sequence.Keys.First());
        }

        private void DownloadGroups()
        {
            foreach (var sequenceKey in Sequences.Keys.Distinct())
            {
                var sequence = Sequences[sequenceKey];
                xArm.DownloadGroup(
                sequence.Keys.First(),
                sequence[sequence.Keys.First()]
                    .Select(x => new List<int>() { x.Time, x.Servo1, x.Servo2, x.Servo3, x.Servo4, x.Servo5, x.Servo6 }).ToList());
            }
        }
        private void ParseControlFile()
        {
            Sequences = new Dictionary<string, Dictionary<int, List<Command>>>();

            if (!File.Exists(ControlFileLocation))
                return;

            using (var reader = new StreamReader(Path.GetFullPath(ControlFileLocation)))
            {
                var CurrentGroupName = "";
                var CurrentGroupNumber = -1;
                var CurrentIndex = 1;
                var sequence = new Dictionary<int, List<Command>>();
                var commands = new List<Command>();
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();

                    var values = line.Split(',').Select(x => x.Trim()).ToList();
                    if (values[0] != CurrentGroupName)
                    {
                        if (commands.Count > 0)
                        {
                            sequence.Add(CurrentGroupNumber, commands);
                            Sequences.Add(CurrentGroupName, sequence);
                        }
                        sequence = new Dictionary<int, List<Command>>();
                        commands = new List<Command>();
                        CurrentGroupName = values[0];
                        if (int.Parse(values[1]) != CurrentGroupNumber)
                        {
                            CurrentGroupNumber = int.Parse(values[1]);
                        }
                        CurrentIndex = 1;
                    }
                    var command = new Command();
                    command.Index = CurrentIndex;
                    command.Time = int.Parse(values[2]);
                    command.Servo1 = int.Parse(values[3]);
                    command.Servo2 = int.Parse(values[4]);
                    command.Servo3 = int.Parse(values[5]);
                    command.Servo4 = int.Parse(values[6]);
                    command.Servo5 = int.Parse(values[7]);
                    command.Servo6 = int.Parse(values[8]);
                    CurrentIndex++;
                    commands.Add(command);
                }

                sequence.Add(CurrentGroupNumber, commands);
                Sequences.Add(CurrentGroupName, sequence);
            }
        }

    }
}
