﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using HidSharp;

namespace OpenTap.Plugins.RoboticArmControl
{
    public class XArmHid
    {
        private static readonly TraceSource log = Log.CreateSource("XArm HID");
        private const int vid = 1155;
        private const int pid = 22352;

        private HidDevice device = null;
        private HidStream stream = null;

        private Dictionary<int, int> groupDelay;

        public XArmHid()
        {
            groupDelay = new Dictionary<int, int>();
        }

        public void Connect(string serialNumber)
        {
            // FindHidDevice
            var device = FindHidDevice(serialNumber);
            // ConnectDevice
            if (device != null)
            {
                var stream = device.Open();
                if (stream != null)
                {
                    this.device = device;
                    this.stream = stream;
                    log.Debug("Opened!");
                    return;
                }
                else
                {
                    throw new Exception("Open failed...");
                }
            }
            throw new Exception($"Unable to find HID device with serial no {serialNumber}.");
        }
        public void Disconnect()
        {
            if (this.stream != null)
            {
                this.stream.Close();
            }
        }
        private HidDevice FindHidDevice(string serialNumber)
        {
            foreach (var hidDevice in HidSharp.DeviceList.Local.GetHidDevices())
            {
                if (hidDevice.VendorID == vid && hidDevice.ProductID == pid && hidDevice.GetSerialNumber() == serialNumber)
                {
                    Console.WriteLine("Found device!");
                    return hidDevice;
                }
            }
            return null;
        }

        public static List<string> Discover()
        {
            var discoveredDevices = new List<string>();
            foreach (var hidDevice in HidSharp.DeviceList.Local.GetHidDevices())
            {
                if (hidDevice.VendorID == vid && hidDevice.ProductID == pid)
                {
                    string sn = hidDevice.GetSerialNumber();
                    Console.WriteLine("Found device {0}", sn);
                    discoveredDevices.Add(sn);
                }
            }
            return discoveredDevices;
        }

        public void DownloadGroup(int groupNumber, List<List<int>> actions)
        {
            groupDelay[groupNumber] = actions.Sum(x => x[0]) + 100;

            var bytesList = BuildDownloadGroup(groupNumber, actions);

            foreach(var bytes in bytesList)
            {
                stream.Write(bytes);
                stream.ReadTimeout = 800;
                byte[] report = stream.Read();
            }
        }
        public void PlayGroup(int groupNumber)
        {            
            byte[] bytes = BuildPlayGroup(groupNumber);
            stream.Write(bytes);

            System.Threading.Thread.Sleep(groupDelay[groupNumber]);

            //WaitForGroupComplete(groupNumber);
        }
        private List<byte[]> BuildDownloadGroup(int groupNumber, List<List<int>> actions )
        {
            var bytesList = new List<byte[]>(actions.Count + 4);

            bytesList.Add(BuildDownloadGroup_Sequence1(groupNumber, actions));
            bytesList.Add(BuildDownloadGroup_Sequence2(groupNumber, actions));
            bytesList.Add(BuildDownloadGroup_Sequence3(groupNumber, actions));
            bytesList.AddRange(BuildDownloadGroup_Sequence4(groupNumber, actions));
            bytesList.Add(BuildDownloadGroup_Sequence5(groupNumber, actions));

            return bytesList;
        }

        private byte[] BuildDownloadGroup_Sequence1(int groupNumber, List<List<int>> actions)
        {
            var bytesList = new List<byte[]>(actions.Count + 4);

            // Byte sequence 1
            // Calculate the number of 0-254 action 'buckets' we need (action 255 / FF is reserved...)
            int numberOfBuckets = ((actions.Count - 1) / 255) + 1;

            var memoryStream = new System.IO.MemoryStream();
            var writer = new System.IO.BinaryWriter(memoryStream);

            writer.Write((byte)0);                              // always 0
            writer.Write((UInt16)0x5555);                       // header
            writer.Write((byte)0);                              // fill in with length when done
            writer.Write((byte)RobotCommand.GroupRunRepeat);    // download group command
            writer.Write((byte)1);                              // sequence number
            writer.Write((byte)groupNumber);                    // group number

            Console.WriteLine(memoryStream.Length);
            for (int fullBucketI = 0; fullBucketI < numberOfBuckets - 1; fullBucketI++)
            {
                writer.Write((byte)0xFF);       // full buckets get 0xFF even though one of them is not used
                Console.WriteLine(memoryStream.Length);
            }
            Console.WriteLine(memoryStream.Length);
            writer.Write((byte)(actions.Count - ((numberOfBuckets - 1) * 255))); // partially full bucket size
            Console.WriteLine(memoryStream.Length);


            memoryStream.Position = 3;
            memoryStream.WriteByte((byte)(memoryStream.Length - 3)); // write the actual length

            byte[] result = memoryStream.ToArray();
            writer.Close();
            memoryStream.Close();
            
            return result;
        }

        private byte[] BuildDownloadGroup_Sequence2(int groupNumber, List<List<int>> actions)
        {
            // Byte sequence 2
            // Calculate the number of 0-254 action 'buckets' we need (action 255 / 0xFF is reserved...)
            int numberOfBuckets = ((actions.Count - 1) / 255) + 1;

            var memoryStream = new System.IO.MemoryStream();
            var writer = new System.IO.BinaryWriter(memoryStream);

            writer.Write((byte)0);                              // always 0
            writer.Write((UInt16)0x5555);                       // header
            writer.Write((byte)0);                              // fill in with length when done
            writer.Write((byte)RobotCommand.GroupRunRepeat);    // download group command
            writer.Write((byte)2);                              // sequence number
            writer.Write((byte)groupNumber);                    // group number
            writer.Write((UInt16)numberOfBuckets);              // number of buckets
            
            memoryStream.Position = 3;
            memoryStream.WriteByte((byte)(memoryStream.Length - 3)); // write the actual length

            byte[] result = memoryStream.ToArray();

            writer.Close();
            memoryStream.Close();

            return result;
        }

        private byte[] BuildDownloadGroup_Sequence3(int groupNumber, List<List<int>> actions)
        {
            // Byte sequence 3
            // Calculate the number of 0-254 action 'buckets' we need (action 255 / 0xFF is reserved...)
            int numberOfBuckets = ((actions.Count - 1) / 255) + 1;

            var memoryStream = new System.IO.MemoryStream();
            var writer = new System.IO.BinaryWriter(memoryStream);

            writer.Write((byte)0);                              // always 0
            writer.Write((UInt16)0x5555);                       // header
            writer.Write((byte)0);                              // fill in with length when done
            writer.Write((byte)RobotCommand.GroupRunRepeat);    // download group command
            writer.Write((byte)3);                              // sequence number
            writer.Write((byte)groupNumber);                    // group number
            writer.Write((byte)0);                              // always 0
            for (int groupI = 0; groupI < numberOfBuckets; groupI++)
            {
                writer.Write((byte)groupI);
            }

            memoryStream.Position = 3;
            memoryStream.WriteByte((byte)(memoryStream.Length - 3)); // write the actual length

            byte[] result = memoryStream.ToArray();
            writer.Close();
            memoryStream.Close();

            return result;
        }

        private List<byte[]> BuildDownloadGroup_Sequence4(int groupNumber, List<List<int>> actions)
        {
            // Byte sequence 4
            // Calculate the number of 0-254 action 'buckets' we need (action 255 / 0xFF is reserved...)
            int numberOfBuckets = ((actions.Count - 1) / 255) + 1;
            var allActions = new List<byte[]>();
            for (int actionI = 0; actionI < actions.Count; actionI++)
            {

                var memoryStream = new System.IO.MemoryStream();
                var writer = new System.IO.BinaryWriter(memoryStream);

                writer.Write((byte)0);                                  // always 0
                writer.Write((UInt16)0x5555);                           // header
                writer.Write((byte)0);                                  // fill in with length when done
                writer.Write((byte)RobotCommand.GroupRunRepeat);        // download group command
                writer.Write((byte)4);                                  // sequence number
                writer.Write((byte)groupNumber);                        // group number

                int bucketNumber = actionI / 255;
                int adjustedActionI = actionI + (actionI / 255) - (bucketNumber * 256);
                writer.Write((byte)bucketNumber);
                writer.Write((byte)adjustedActionI);                // remember 255 / 0xFF is reserved
                writer.Write((byte)0x0B);                           // magic, pure magic
                writer.Write((UInt16)actions[actionI][0]);
                for (int jointI = 1; jointI < actions[actionI].Count; jointI++)
                {
                    writer.Write((byte)jointI);                     // joint index
                    writer.Write((UInt16)actions[actionI][jointI]); // joint value
                }

                memoryStream.Position = 3;
                memoryStream.WriteByte((byte)(memoryStream.Length - 3)); // write the actual length

                allActions.Add(memoryStream.ToArray());
               
                writer.Close();
                memoryStream.Close();
            }

            return allActions;
        }

        private byte[] BuildDownloadGroup_Sequence5(int groupNumber, List<List<int>> actions)
        {
            // Byte sequence 5
            // Calculate the number of 0-254 action 'buckets' we need (action 255 / 0xFF is reserved...)
            int numberOfBuckets = ((actions.Count - 1) / 255) + 1;

            var memoryStream = new System.IO.MemoryStream();
            var writer = new System.IO.BinaryWriter(memoryStream);

            writer.Write((byte)0);                                  // always 0
            writer.Write((UInt16)0x5555);                           // header
            writer.Write((byte)0);                                  // fill in with length when done
            writer.Write((byte)RobotCommand.GroupRunRepeat);        // download group command
            writer.Write((byte)5);                                  // sequence number
            writer.Write((byte)groupNumber);                        // group number

            memoryStream.Position = 3;
            memoryStream.WriteByte((byte)(memoryStream.Length - 3)); // write the actual length

            byte[] result = memoryStream.ToArray();
            writer.Close();
            memoryStream.Close();

            return result;
        }

        private byte[] BuildPlayGroup(int groupNumber)
        {
            var memoryStream = new System.IO.MemoryStream();
            var writer = new System.IO.BinaryWriter(memoryStream);

            writer.Write((byte)0);                                  // always 0
            writer.Write((UInt16)0x5555);                           // header
            writer.Write((byte)0);                                  // fill in with length when done
            writer.Write((byte)RobotCommand.GroupRun);              // run group command
            writer.Write((byte)groupNumber);                        // group number
            writer.Write((UInt16)1);                                // do it once

            memoryStream.Position = 3;
            memoryStream.WriteByte((byte)(memoryStream.Length - 3)); // write the actual length

            byte[] result = memoryStream.ToArray();
            writer.Close();
            memoryStream.Close();

            return result;
        }

        public void Reset()
        {
            ServoMoveAll(1000, 500, 500, 500, 500, 500, 500);
        }

        public void ServoMoveAll(int timeMs, int servo1, int servo2, int servo3, int servo4, int servo5, int servo6)
        {
            var bytes = BuildServoMoveAll(timeMs, servo1, servo2, servo3, servo4, servo5, servo6);
            stream.Write(bytes);
            
            System.Threading.Thread.Sleep(timeMs);
        }
        private byte[] BuildServoMoveAll(int timeMs, int servo1, int servo2, int servo3, int servo4, int servo5, int servo6)
        {
            var memoryStream = new System.IO.MemoryStream();
            var writer = new System.IO.BinaryWriter(memoryStream);

            writer.Write((byte)0);                                  // always 0
            writer.Write((UInt16)0x5555);                           // header
            writer.Write((byte)0);                                  // fill in with length when done
            writer.Write((byte)3);                                  // run group command
            writer.Write((byte)6);                                  // servos 1-6
            writer.Write((UInt16)timeMs);                           // time to complete action
            writer.Write((byte)1);                                  // servo 1
            writer.Write((UInt16)servo1);                           // servo 1 value
            writer.Write((byte)2);                                  // servo 2
            writer.Write((UInt16)servo2);                           // servo 2 value
            writer.Write((byte)3);                                  // servo 3
            writer.Write((UInt16)servo3);                           // servo 3 value
            writer.Write((byte)4);                                  // servo 4
            writer.Write((UInt16)servo4);                           // servo 4 value
            writer.Write((byte)5);                                  // servo 5
            writer.Write((UInt16)servo5);                           // servo 5 value
            writer.Write((byte)6);                                  // servo 6
            writer.Write((UInt16)servo6);                           // servo 6 value

            memoryStream.Position = 3;
            memoryStream.WriteByte((byte)(memoryStream.Length - 3)); // write the actual length

            byte[] result = memoryStream.ToArray();
            writer.Close();
            memoryStream.Close();

            return result;
        }

        public void ServoMove(int servoNumber, int servoValue)
        {
            var bytes = BuildServoMove(servoNumber, servoValue);
            stream.Write(bytes);

            System.Threading.Thread.Sleep(250);
        }
        private byte[] BuildServoMove(int servoNumber, int servoValue)
        {
            var memoryStream = new System.IO.MemoryStream();
            var writer = new System.IO.BinaryWriter(memoryStream);

            writer.Write((byte)0);                                  // always 0
            writer.Write((UInt16)0x5555);                           // header
            writer.Write((byte)0);                                  // fill in with length when done
            writer.Write((byte)3);                                  // run group command
            writer.Write((UInt16)1);                                // 1 ms, ya!!!
            writer.Write((byte)0);                                 // magic 0
            writer.Write((byte)servoNumber);                        // servo #
            writer.Write((UInt16)servoValue);                       // servo # value

            memoryStream.Position = 3;
            memoryStream.WriteByte((byte)(memoryStream.Length - 3)); // write the actual length

            byte[] result = memoryStream.ToArray();
            writer.Close();
            memoryStream.Close();

            return result;
        }


        public void WaitForGroupComplete(int groupNumber)
        {
            var bytes = BuildWaitForGroupComplete(groupNumber);
            stream.Write(bytes);
            stream.ReadTimeout = 10000;
            byte[] report = stream.Read();
        }
        private byte[] BuildWaitForGroupComplete(int groupNumber)
        {
            var memoryStream = new System.IO.MemoryStream();
            var writer = new System.IO.BinaryWriter(memoryStream);

            writer.Write((byte)0);                                  // always 0
            writer.Write((UInt16)0x5555);                           // header
            writer.Write((byte)0);                                  // fill in with length when done
            writer.Write((byte)8);                                  // run group command
            writer.Write((byte)groupNumber);                        // group number
            writer.Write((UInt16)1);                                // do it once

            memoryStream.Position = 3;
            memoryStream.WriteByte((byte)(memoryStream.Length - 3)); // write the actual length

            byte[] result = memoryStream.ToArray();
            writer.Close();
            memoryStream.Close();

            return result;
        }

        public enum RobotCommand
        {
            MultiServoMove = 3,  // (byte)count (ushort)time { (byte)id (ushort)position }
            GroupRunRepeat = 5,  // (byte)group[255=all] (byte)times 
            GroupRun = 6,  // (byte)group (ushort)count[0=continuous]
            GroupStop = 7,  // -none-
            GroupErase = 8,  // (byte)group[255=all]
            GroupSpeed = 11, // (byte)group (ushort)percentage
            xServoOffsetWrite = 12,
            xServoOffsetRead = 13,
            xServoOffsetAdjust = 14,
            GetBatteryVoltage = 15, // -none-; (ushort)millivolts
            ServoOff = 20, // (byte)count { (byte)id }
            ServoPositionRead = 21, // (byte)count { (byte)id }; (byte)count { (byte)id (byte)offset }
            ServoPositionWrite = 22, // (byte)count { (byte)id }
            ServoOffsetRead = 23, // (byte)count { (byte)id }; (byte)count { (byte)id (byte)offset }
            ServoOffsetWrite = 24, // (byte)id (ushort)value
            BusServoMoroCtrl = 26, // (byte)id (byte)??? (ushort)speed
            BusServoInfoWrite = 27, // (byte)id (ushort)pos_min (ushort)pos_max (ushort)volt_min (ushort)volt_max
                                    //         (ushort)temp_max (byte)led_status (byte)led_warning
            BusServoInfoRead = 28  // -none-; (byte)id (ushort)pos_min (ushort)pos_max (ushort)volt_min (ushort)volt_max 
                                   //         (ushort)temp_max (byte)led_status (byte)led_warning (byte)dev_offset
                                   //         (ushort)pos (byte)temp (ushort)volt
        }
    }
}
