﻿using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;

namespace OpenTap.Plugins.RoboticArmControl
{
    [Display("Arm Control", Group: "Robotic Arm Control", Description: "Base Class for Robitic Arm Movements")]
    public abstract class ArmControl : TestStep
    {
        #region Settings
        [Display("Robotic Arm")]
        public IRoboticArm Arm { get; set; }

        [Display("DUT Type")]
        [AvailableValues("DutTypes")]
        public string DutType { get; set; }

        [Browsable(false)]
        public IEnumerable<string> DutTypes => InstrumentSettings.Current.OfType<IRoboticArm>().SelectMany(x => x.GetDutTypes()).Distinct();


        [Display("Pickup Location")]
        [AvailableValues("Locations")]
        public string DutLocationName { get; set; }

        [Browsable(false)]
        public IEnumerable<string> Locations => InstrumentSettings.Current.OfType<IRoboticArm>().SelectMany(x => x.GetLocations()).Distinct();

        #endregion

        public ArmControl()
        {            
            DutType = DutTypes.FirstOrDefault();
            DutLocationName = Locations.FirstOrDefault();
        }

        public override void Run()
        {
            // ToDo: Add test case code.
            RunChildSteps(); //If the step supports child steps.
            UpgradeVerdict(Verdict.Pass);
        }
    }
}
