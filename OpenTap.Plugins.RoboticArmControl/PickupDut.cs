﻿using System.ComponentModel;

namespace OpenTap.Plugins.RoboticArmControl
{
    [Display("Pickup DUT", Group: "Robotic Arm Control", Description: "Pickup a DUT from a known location")]
    [Browsable(true)]
    public class PickupDut : ArmControl
    {
        

        public PickupDut()
        {
            
        }

        public override void Run()
        {
            Arm.PlayAction(DutType, DutLocationName);
            Log.Debug("DUT picked up");
            UpgradeVerdict(Verdict.Pass);
        }
    }
}
