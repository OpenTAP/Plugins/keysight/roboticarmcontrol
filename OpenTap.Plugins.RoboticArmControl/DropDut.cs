﻿using System.ComponentModel;

namespace OpenTap.Plugins.RoboticArmControl
{
    [Display("Drop DUT", Group: "Robotic Arm Control", Description: "Insert a description here")]
    [Browsable(true)]
    public class DropDut : ArmControl
    {
        #region Settings
        [Display("DUT Serial Number")]
        public Input<string> DutSerialNumber { get; set; }

        [Browsable(false)]
        public new string DutType { get; set; }

        [Output]
        public string TestType { get; private set; }

        
        #endregion

        public DropDut()
        {
            DutSerialNumber = new Input<string>();
        }

        public override void Run()
        {
            
            if(DutSerialNumber.Value.Contains("FR1") == true)
            {
                TestType = "FR1";
                DutType = "FR1";
                Arm.PlayAction( DutType, DutLocationName);
                UpgradeVerdict(Verdict.Pass);
            }
            else if(DutSerialNumber.Value.Contains("FR2") == true)
            {
                TestType = "FR1+FR2";
                DutType = "FR2";
                Arm.PlayAction(DutType, DutLocationName);
                UpgradeVerdict(Verdict.Pass);
            }
            else
            {
                Arm.PlayAction(DutType, DutLocationName);
                UpgradeVerdict(Verdict.Fail);
            }
                    
          
        }
    }
}
