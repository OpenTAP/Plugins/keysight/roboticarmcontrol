﻿namespace OpenTap.Plugins.RoboticArmControl
{
    [Display("Move Joint", Group: "Robotic Arm Control", Description: "Adjust a single joint")]
    public class MoveJoint : TestStep
    {
        [Display("Robotic Arm")]
        public IRoboticArm Arm { get; set; }

        #region Settings
        [Display("Joint ID")]
        public int JointID { get; set; }

        public double Position { get; set; }
        #endregion

        public MoveJoint()
        {
            // ToDo: Set default values for properties / settings.
        }

        public override void Run()
        {
            // ToDo: Add test case code.
            Arm.MoveJoint(JointID, Position);
            UpgradeVerdict(Verdict.Pass);
        }
    }
}
