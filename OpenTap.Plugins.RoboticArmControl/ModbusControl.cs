﻿using System;
using System.Collections.Generic;
using AMWD.Modbus.Tcp.Client;
using ModbusCommon = AMWD.Modbus.Common;
using System.Threading.Tasks;

namespace OpenTap.Plugins.RoboticArmControl
{

    public class ModbusControl
    {
        private ModbusClient modbusClient; 

        // host = RC3 Simulator hostname/ip, port = remote port of ModbusTCP server
        public ModbusControl(string host, int port)
        {
            modbusClient = new ModbusClient(host, port); 
        }

        /// <summary>
        /// Open procedure for the instrument.
        /// </summary>
        public async void Open()
        {
            await modbusClient.Connect();
        }

        public bool IsClientConnected()
        {
            return modbusClient.IsConnected;
        }

        public async Task<List<ModbusCommon.Structures.Register>> ReadAllJointAngles(byte devId)
        {
            var info = await modbusClient.ReadInputRegisters(devId, 270, 6);
            return info;
        }

        public async Task<List<ModbusCommon.Structures.Register>> ReadJointAngle(byte devId, Joints joint)
        {
            ushort jointNumber = (ushort)joint;
            ushort jointRegister = (ushort)(270 + jointNumber);
            var info = await modbusClient.ReadInputRegisters(devId, jointRegister, 1);
            return info;
        }

        public async Task<bool> WriteJointAngle(byte devId, Joints joint, ushort angleNumber)
        {
            ushort jointNumber = (ushort)joint;
            ushort jointRegister = (ushort)(270 + jointNumber);
            return await WriteRegister(devId, jointRegister, angleNumber); 
        }

        public async Task<bool> WriteRegister(byte devId, int regNo, int value)
        {
            ModbusCommon.Structures.Register register = new ModbusCommon.Structures.Register();
            register.Address = (ushort)regNo;
            register.Value = (ushort)value;
            bool success = await modbusClient.WriteSingleRegister(devId, register);
            return success;
        }

        public async Task<List<ModbusCommon.Structures.Register>> ReadRobotStateRegister(byte devId)
        {
            var info = await modbusClient.ReadInputRegisters(devId, 258, 1);
            return info;
        }

        /// <summary>
        /// Close procedure for the instrument.
        /// </summary>
        public async void Close()
        {
            await modbusClient.Disconnect();
            modbusClient.Dispose();
        }
    }
}
