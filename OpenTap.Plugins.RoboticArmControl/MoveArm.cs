﻿using System.Collections.Generic;
using System.Linq;

namespace OpenTap.Plugins.RoboticArmControl
{
    [Display("Move Arm", Group: "Robotic Arm Control", Description: "Generic Arm movement based on list of points")]
    public class MoveArm : TestStep
    {
        [Display("Robotic Arm")]
        public IRoboticArm Arm { get; set; }

        #region Settings
        [Display("Positions List")]
        public List<double> ServoPositions { get; set; }
        #endregion

        public MoveArm()
        {
            // ToDo: Set default values for properties / settings.
        }

        public override void Run()
        {
            Arm.Move(ServoPositions);
            UpgradeVerdict(Verdict.Pass);
        }
    }
}
