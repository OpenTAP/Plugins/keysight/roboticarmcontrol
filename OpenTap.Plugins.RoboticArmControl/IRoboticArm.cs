﻿using System.Collections.Generic;
using System.ComponentModel;

namespace OpenTap.Plugins.RoboticArmControl
{
    public interface IRoboticArm : IInstrument
    {

        #region Settings
        string ControlFileLocation { get; set; }
        string Address { get; set; }
        #endregion

        void PlayAction(string dutType, string dutLocationName);

        void Move(List<double> position);

        void MoveJoint(int joinID, double position);

        void Reset();

        List<string> GetLocations();

        List<string> GetDutTypes();

    }
}
