﻿using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace OpenTap.Plugins.RoboticArmControl
{
    public enum Joints { Base, Shoulder, Elbow, Wrist1, Wrist2, Wrist3 };

    [Display("Universal Robot Arm Simulator", Group: "Robotic Arm Control", Description: "Example of controling a Modbus based Robotic Arm")]
    public class UniversalRobotArmSimulator : Instrument, IRoboticArm
    {

        private class Command
        {
            public double Servo1;
            public double Servo2;
            public double Servo3;
            public double Servo4;
            public double Servo5;
            public double Servo6;
            public int Time;
            public double Acceleration;
            public double Velocity;
            public int Index;
        };

        [Display("Control File Location")]
        [FilePath]
        public string ControlFileLocation { get; set; }

        [Display("Remote Host IP Address")]
        public string Address { get; set; }

        [Display("Remote Host Port")]
        public int RemoteHostPort { get; set; }

        [Display("Reset Location")]
        public List<double> ResetLocation { get; set; }

        private ModbusSocketServer SocketServer;
        private ModbusControl ModbusClient;
      

        private Dictionary<string, Dictionary<int, List<Command>>> Sequences;


        public UniversalRobotArmSimulator()
        {
            Name = "URSim";
            Address = "localhost";
            RemoteHostPort = 502;
            ResetLocation = new List<double>() { -2, -1.5, -0.5, -1, -1, -1 };
        }

        public override void Open()
        {
            SocketServer = new ModbusSocketServer(Address);
            ModbusClient = new ModbusControl(Address, RemoteHostPort);
            ModbusClient.Open();
            Reset();
            ParseControlFile();
            base.Open();
            IsConnected = ModbusClient.IsClientConnected();
        }

        public override void Close()
        {
            SocketServer.DisconnectSocket();
            base.Close();
        }

        public void Move(List<double> position)
        {
            // default is 1 second
            Move(position, 1);
        }

        public void Move(List<double> position, double timeOfMovement)
        {
            string command = $"movej([{position[0]}, {position[1]}, {position[2]}, {position[3]}, {position[4]}, {position[5]}]," +
                $"a=1.0, v=1.0, t={timeOfMovement})\n";

            SocketServer.SendCommand(command);
        }

        public void MoveJoint(int joinID, double position)
        {
            throw new NotImplementedException();
        }

        public void Reset()
        {
            Move(ResetLocation);
        }

        public void ScanBarcode(string dutType)
        {
            throw new NotImplementedException();
        }

        public async Task<List<double>> ReadAngles()
        {
            var angleRegisters = await ModbusClient.ReadAllJointAngles(1);
            List<double> angles = new List<double>();
            foreach (var register in angleRegisters)
            {
                angles.Add(register.Value);
            }

            return angles;
        }

        public async Task<double> ReadJointAngle(Joints joint)
        {
            var angleList = await ModbusClient.ReadJointAngle(1, joint);
            return angleList[0].Value; 
        }

        private void ParseControlFile()
        {
            Sequences = new Dictionary<string, Dictionary<int, List<Command>>>();

            if (!File.Exists(ControlFileLocation))
                return;

            using (var reader = new StreamReader(Path.GetFullPath(ControlFileLocation)))
            {
                var CurrentGroupName = "";
                var CurrentGroupNumber = -1;
                var CurrentIndex = 1;
                var sequence = new Dictionary<int, List<Command>>();
                var commands = new List<Command>();
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();

                    var values = line.Split(',').Select(x => x.Trim()).ToList();
                    if (values[0] != CurrentGroupName)
                    {
                        if (commands.Count > 0)
                        {
                            sequence.Add(CurrentGroupNumber, commands);
                            Sequences.Add(CurrentGroupName, sequence);
                        }
                        sequence = new Dictionary<int, List<Command>>();
                        commands = new List<Command>();
                        CurrentGroupName = values[0];
                        if (int.Parse(values[1]) != CurrentGroupNumber)
                        {
                            CurrentGroupNumber = int.Parse(values[1]);
                        }
                        CurrentIndex = 1;
                    }
                    var command = new Command();
                    command.Index = CurrentIndex;
                    command.Time = int.Parse(values[2]);
                    command.Servo1 = double.Parse(values[3]);
                    command.Servo2 = double.Parse(values[4]);
                    command.Servo3 = double.Parse(values[5]);
                    command.Servo4 = double.Parse(values[6]);
                    command.Servo5 = double.Parse(values[7]);
                    command.Servo6 = double.Parse(values[8]);
                    //command.Acceleration = double.Parse(values[9]);
                    //command.Velocity = double.Parse(values[10]);
                    CurrentIndex++;
                    commands.Add(command);
                }

                sequence.Add(CurrentGroupNumber, commands);
                Sequences.Add(CurrentGroupName, sequence);
            }
        }

        public void PlayAction(string dutType, string dutLocationName)
        {
            var sequence = Sequences[$"{dutType}:{dutLocationName}"];
            foreach(var command in sequence.Values.First())
            {
                Move(new List<double>() {command.Servo1, command.Servo2, command.Servo3, command.Servo4, command.Servo5, command.Servo6 });
                Thread.Sleep(command.Time + 1);
            }
        }

        public List<string> GetLocations()
        {
            ParseControlFile();
            return Sequences.Keys.Distinct().Select(x => x.Split(':')[1]).ToList();
        }

        public List<string> GetDutTypes()
        {
            ParseControlFile();
            return Sequences.Keys.Distinct().Select(x => x.Split(':')[0]).ToList();
        }
    }
}
